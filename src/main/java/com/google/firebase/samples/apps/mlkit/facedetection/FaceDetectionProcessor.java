// Copyright 2018 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
package com.google.firebase.samples.apps.mlkit.facedetection;

//import android.support.annotation.NonNull;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.samples.apps.mlkit.FrameMetadata;
import com.google.firebase.samples.apps.mlkit.GraphicOverlay;
import com.google.firebase.samples.apps.mlkit.MyJobIntentService;
import com.google.firebase.samples.apps.mlkit.VisionProcessorBase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Face Detector Demo.
 */
public class FaceDetectionProcessor extends VisionProcessorBase<List<FirebaseVisionFace>> {

    private static final String TAG = "FaceDetectionProcessor";
    private static ByteBuffer byteBuffer;

    private final FirebaseVisionFaceDetector detector;

    static int oldFrame = -1, thisFrame = -1;

    private Context context;

//  private ByteBuffer imageContainsFace;

    public FaceDetectionProcessor(Context context) {
        this.context = context;
        FirebaseVisionFaceDetectorOptions options =
                new FirebaseVisionFaceDetectorOptions.Builder()
                        .setClassificationType(FirebaseVisionFaceDetectorOptions.ALL_CLASSIFICATIONS)
                        .setTrackingEnabled(true)
                        .build();


        detector = FirebaseVision.getInstance().getVisionFaceDetector(options);
    }

    public static ByteBuffer getByteBuffer() {
        if (byteBuffer != null) return byteBuffer;
        return null;
    }

    @Override
    public void stop() {
        try {
            detector.close();
        } catch (IOException e) {
            Log.e(TAG, "Exception thrown while trying to close Face Detector: " + e);
        }
    }

    @Override
    protected Task<List<FirebaseVisionFace>> detectInImage(FirebaseVisionImage image) {
        return detector.detectInImage(image);
    }

    @Override
    protected void onSuccess(
            @NonNull List<FirebaseVisionFace> faces,
            @NonNull FrameMetadata frameMetadata,
            @NonNull GraphicOverlay graphicOverlay,
            @NonNull Bitmap imageContainsFace) {
        graphicOverlay.clear();

        for (int i = 0; i < faces.size(); ++i) {
            FirebaseVisionFace face = faces.get(i);
            FaceGraphic faceGraphic = new FaceGraphic(graphicOverlay);
            graphicOverlay.add(faceGraphic);
            faceGraphic.updateFace(face, frameMetadata.getCameraFacing());

            thisFrame = face.getTrackingId();
            if (oldFrame != thisFrame && face.getLeftEyeOpenProbability() > 0.85 && face.getRightEyeOpenProbability() > 0.85) {

                Intent serviceIntent = new Intent();
//        byte[] arr = new byte[imageContainsFace.remaining()];
//        imageContainsFace.get(arr);
                serviceIntent.putExtra("image", 5);
//        Log.e(TAG, "-----------------------> ");
//        Toast.makeText(context, "anass", Toast.LENGTH_SHORT).show();
//                byteBuffer = imageContainsFace;

                String pathToImage = saveImageInDirectory(imageContainsFace);
                serviceIntent.putExtra("pathToImage", pathToImage);
                JobIntentService.enqueueWork(context, MyJobIntentService.class, 351, serviceIntent);

                oldFrame = thisFrame;
            }

            /* TODO
             *  whenever new face detected send it to be processed*/

        }
    }

    String currentPhotoPath;

    private String saveImageInDirectory(Bitmap imageContainsFace) {
//        byte[] imageBytes = new byte[imageContainsFace.remaining()];
//        imageContainsFace.get(imageBytes);
//        final Bitmap bmp = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
//        Log.e(TAG, "----------> "+bmp);

        // creating empty image file with jpg suffix
        File image = createImageFile();

        try {
//            Log.e(TAG, "---------->"+image.toString());
            imageContainsFace.compress(Bitmap.CompressFormat.JPEG, 90, new FileOutputStream(image));
            return currentPhotoPath;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return null;
    }



    private File createImageFile() {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = null;
        try {
            image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onFailure(@NonNull Exception e) {
        Log.e(TAG, "Face detection failed " + e);
    }
}
