package com.google.firebase.samples.apps.mlkit;

import android.content.Context;
import android.content.Intent;
import android.nfc.Tag;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.google.firebase.samples.apps.mlkit.facedetection.FaceDetectionProcessor;
import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.visual_recognition.v3.model.ClassifiedImages;
import com.ibm.watson.visual_recognition.v3.model.ClassifyOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MyJobIntentService extends JobIntentService {
    private static final String TAG = "MyJobIntentService";

//    static void enqueueWork(Context context, Intent work) {
////        Log.e(TAG, "**************"+work.getIntExtra("image", -1));
//
//        enqueueWork(context, MyJobIntentService.class, 351, work);
//    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

//        Log.e(TAG, "**************"+FaceDetectionProcessor.getByteBuffer());

//        new ClassifierHandler().execute(FaceDetectionProcessor.getByteBuffer());

        String pathToImage = intent.getStringExtra("pathToImage");
        new ClassifierHandler().execute(pathToImage);
        if (isStopped()) return;

        SystemClock.sleep(5000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }


    class ClassifierHandler extends AsyncTask<String, Void, ClassifiedImages> {

        @Override
        protected ClassifiedImages doInBackground(String... paths) {
            Authenticator authenticator = new IamAuthenticator("3e0TOxX-D5vbdCSmZo1hgbrUP-YiaXtDdYYXAOzOxWQ3");
            VisualRecognition service = new VisualRecognition("2018-03-19", authenticator);
            service.setServiceUrl("https://api.us-south.visual-recognition.watson.cloud.ibm.com/instances/7133b482-5203-43ff-bf34-9524ad12727a");

            ClassifyOptions options = null;
            try {
                options = new ClassifyOptions.Builder()
                        .imagesFile(new File(paths[0]))
                        .classifierIds(Arrays.asList("DefaultCustomModel_1649467712"))
                        .build();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            ClassifiedImages result = service.classify(options).execute().getResult();
            return result;
        }

        @Override
        protected void onPostExecute(ClassifiedImages classifiedImages) {
            Log.e(TAG, classifiedImages.toString());
            String personName = retrieveNameFromJsonResponse(classifiedImages);
            declarePersonAsPresent(personName);
        }
    }

    private void declarePersonAsPresent(String personName) {
        Log.e(TAG, personName+ "is present");
    }

    private String retrieveNameFromJsonResponse(ClassifiedImages classifiedImages) {
//        try {
//            JSONObject jsonObject = new JSONObject(classifiedImages.toString());
//            JSONArray images = jsonObject.getJSONArray("images");
//
////            String personName = images.getJSONObject(0).getJSONArray("classifiers").getJSONObject(0).getJSONArray("classes").getJSONObject(0).getString("class");
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        Pattern pattern = Pattern.compile("[A-Za-z]+.zip");
        Matcher matcher = pattern.matcher(classifiedImages.toString());
        if (matcher.find()){
            String personName = matcher.group();
            return personName.substring(0, personName.length()-4);
        }
        return null;
    }
}
