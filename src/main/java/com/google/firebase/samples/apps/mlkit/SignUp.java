package com.google.firebase.samples.apps.mlkit;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {

    private EditText input_FirstName, input_LastName, input_username, input_email, input_password, input_passwordC;
    private String firstName, lastName, username, email, password, passwordC;
    private TextInputLayout input_layout_FirstName, input_layout_LastName, input_layout_Username, input_layout_Email, input_layout_Password, input_layout_PasswordC;
    private ProgressDialog progressDialog;
    private AlertDialog alertDialog;
    private CardView signUpButton;
    private SessionManagement sessionManagement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        sessionManagement = new SessionManagement(getApplicationContext());

        input_FirstName = findViewById(R.id.input_FirstName);
        input_LastName  = findViewById(R.id.input_LastName);
        input_username  = findViewById(R.id.input_username);
        input_email     = findViewById(R.id.input_email);
        input_password  = findViewById(R.id.input_password);
        input_passwordC = findViewById(R.id.input_passwordC);

        input_layout_FirstName  = findViewById(R.id.input_layout_FirstName);
        input_layout_LastName   = findViewById(R.id.input_layout_LastName);
        input_layout_Username   = findViewById(R.id.input_layout_username);
        input_layout_Email      = findViewById(R.id.input_layout_email);
        input_layout_Password   = findViewById(R.id.input_layout_password);
        input_layout_PasswordC  = findViewById(R.id.input_layout_passwordC);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        alertDialog = new AlertDialog.Builder(this).create();
        signUpButton = findViewById(R.id.sign_up);
        signUpButton.setOnClickListener(v -> {
            signUp();
        });

    }

    public void signUp() {

        firstName   = input_FirstName.getText().toString().trim();
        lastName    = input_LastName.getText().toString().trim();
        username    = input_username.getText().toString().trim();
        email       = input_email.getText().toString().trim();
        password    = input_password.getText().toString().trim();
        passwordC   = input_passwordC.getText().toString().trim();

        if(!firstName.isEmpty() && !lastName.isEmpty() && !username.isEmpty() && !email.isEmpty() && !password.isEmpty() && !passwordC.isEmpty()){
            checkSignUp(firstName, lastName, username, email, password, passwordC);
        }else{
//            Toast.makeText(getApplicationContext(), "f :"+firstName+" l:"+lastName+" u:"+username+" e:"+email+" p:"+password, Toast.LENGTH_LONG).show();
            input_layout_FirstName.setError(getString(R.string.err_msg_required));
            input_layout_LastName.setError(getString(R.string.err_msg_required));
            input_layout_Username.setError(getString(R.string.err_msg_required));
            input_layout_Email.setError(getString(R.string.err_msg_required));
            input_layout_Password.setError(getString(R.string.err_msg_required));
            input_layout_PasswordC.setError(getString(R.string.err_msg_required));

            input_FirstName.setText("");
            input_LastName.setText("");
            input_username.setText("");
            input_email.setText("");
            input_password.setText("");
            input_passwordC.setText("");

            requestFocus(input_FirstName);
        }
    }

    private void checkSignUp(final String firstName, final String lastName, final String username, final String email, final String password, String passwordC) {


        if (!passwordC.equals(password)) {
//            Toast.makeText(getApplicationContext(), "password is not the same ", Toast.LENGTH_LONG).show();
            input_layout_PasswordC.setError(getString(R.string.err_msg_not_same));
            input_passwordC.setText("");
            requestFocus(input_passwordC);
            return;
        } else {

            progressDialog.setTitle("Registering...");
            progressDialog.setMessage("Please wait !");
            if(!progressDialog.isShowing()) progressDialog.show();

            int param1 = Request.Method.POST;
            String param2 = sessionManagement.getRootIp()+URLs.URL_REGISTER;
            Response.Listener<String> param3 = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
//                    try {
//                        //converting response to json object
//                        JSONObject obj = new JSONObject(response);
//                        //if no error in response
//                        if (!obj.getBoolean("error")) {
//                            Toast.makeText(getApplicationContext(), "\"went successfully", Toast.LENGTH_LONG).show();
//                            Intent tohomeUpActivity = new Intent(SignUp.this, Home.class);
//                            startActivity(tohomeUpActivity);
//                            finish();
//                        } else if (obj.getBoolean("error")) {
//                            Toast.makeText(getApplicationContext(), "dkfjghkdfjg", Toast.LENGTH_LONG).show();
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }

                    if(progressDialog.isShowing()) progressDialog.dismiss();

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean error = jsonObject.getBoolean("error");
                        if(error){
                            String err_code = jsonObject.getString("err_code");
                            String error_msg = jsonObject.getString("error_msg");

                            if(err_code.equals("err_email")){
                                input_layout_Email.setError(error_msg);
                                requestFocus(input_layout_Email);
                            }
                            if(err_code.equals("err_username")){
                                input_layout_Username.setError(error_msg);
                                requestFocus(input_layout_Username);
                            }
                        }else{
                            alertDialog.setTitle("Success");
                            alertDialog.setMessage("Registration Complete\nWe will redirect you to login interface to login");
                            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    alertDialog.dismiss();
                                    SignUp.this.finish();
                                }
                            });
                            alertDialog.setOnShowListener(e -> alertDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.colorPrimaryDark)));
                            alertDialog.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Json: "+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            };

            Response.ErrorListener param4 = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                }
            };

            StringRequest stringRequest = new StringRequest(param1, param2, param3, param4) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("firstName", firstName);
                    params.put("lastName", lastName);
                    params.put("username", username);
                    params.put("email", email);
                    params.put("password", password);
                    return params;
                }
            };
            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }
    }

    public void backToLogin(View view) {
        finish();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
}
