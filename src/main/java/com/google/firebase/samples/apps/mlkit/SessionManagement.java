package com.google.firebase.samples.apps.mlkit;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by ANASS on 01/04/2018.
 */

public class SessionManagement {

    private static String TAG = SessionManagement.class.getSimpleName();
    public static final String FIRST_NAME = "FIRST NAME";
    public static final String LAST_NAME = "LAST NAME";
    public static final String ID = "ID";
    public static final String EMAIL = "Email";
    public static final String IP_ADDRESS = "IP";

    private SharedPreferences sharedPreferences;

    private Context context;

    private SharedPreferences.Editor editor;

    private static final String PREF_NAME = "Login";

    private static final String KEY_IS_LOGGEDIN = "is logged in";

    private int PRIVATE_MODE = 0;

    public SessionManagement(Context context){
        this.context = context;
        sharedPreferences = this.context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void setLogin(Boolean isLoggedIn){
        editor.putBoolean(KEY_IS_LOGGEDIN, isLoggedIn);
        editor.commit();
    }

    public void setInfos(String firstName, String lastName, int id, String email){
        editor.putString(FIRST_NAME, firstName);
        editor.putString(LAST_NAME, lastName);
        editor.putString(ID, Integer.toString(id));
        editor.putString(EMAIL, email);
        editor.commit();
    }

    public void setIpAddress(String ipAddress){
        editor.putString(IP_ADDRESS, "http://"+ipAddress+"/pillsApp/User/");
        editor.commit();
    }

    public String getRootIp(){
        HashMap<String, String> info = getInfos();
        return info.get(IP_ADDRESS);
    }

    public HashMap<String, String> getInfos(){
        HashMap<String, String> userInfos = new HashMap<>();

        userInfos.put(FIRST_NAME, sharedPreferences.getString(FIRST_NAME, null));
        userInfos.put(LAST_NAME, sharedPreferences.getString(LAST_NAME, null));
        userInfos.put(ID, sharedPreferences.getString(ID, null));
        userInfos.put(EMAIL, sharedPreferences.getString(EMAIL, null));
        userInfos.put(IP_ADDRESS, sharedPreferences.getString(IP_ADDRESS, null));

        return userInfos;
    }

    public boolean isLoggedIn(){
        return sharedPreferences.getBoolean(KEY_IS_LOGGEDIN, false);
    }

    public void logOut(){
        editor.clear();
        editor.commit();
    }
}
