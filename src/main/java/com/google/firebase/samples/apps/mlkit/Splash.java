package com.google.firebase.samples.apps.mlkit;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;

public class Splash extends AppCompatActivity {

    ImageView imageView;
    private SessionManagement sessionManagement;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        sessionManagement = new SessionManagement(getApplicationContext());


        AnimationSet set = new AnimationSet(true);

        Animation trAnimation = new TranslateAnimation(0, 0, 0, -250);
        trAnimation.setDuration(2000);

        set.addAnimation(trAnimation);

        Animation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(3000);
        set.addAnimation(anim);

        imageView = findViewById(R.id.pill);
        imageView.setAnimation(set);

        final Intent toLoginActivity = new Intent(this, Login.class);
        final Intent toHomeActivity = new Intent(this, ChooserActivity.class);

        set.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
//                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams)imageView.getLayoutParams();
//                params.bottomMargin += 500;
//                imageView.setLayoutParams(params);
                imageView.setVisibility(View.GONE);
                if(sessionManagement.isLoggedIn()){
                    startActivity(toHomeActivity);
                    finish();
                }else {
                    startActivity(toLoginActivity);
                    finish();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });




//        Thread timer = new Thread(){
//            @Override
//            public void run() {
//                try {
//                    sleep(5000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }finally {
//                    startActivity(toLoginActivity);
//                    finish();
//                }
//            }
//        };
//
//        timer.start();
    }

//    @Override
//    protected void onResume() {
//        super.onResume();
////        Toast.makeText(this, "resume", Toast.LENGTH_LONG).show();
//        if(sessionManagement.isLoggedIn()){
//            startActivity(new Intent(this, Home.class));
//            finish();
//        }
//    }
}
