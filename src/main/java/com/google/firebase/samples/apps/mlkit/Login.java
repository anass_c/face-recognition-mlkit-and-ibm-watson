package com.google.firebase.samples.apps.mlkit;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.text.format.Formatter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    private TextInputLayout usernameInputLayout, passwordInputLayout;
    EditText input_name, input_password;
    private ProgressDialog progressDialog;
    private AlertDialog alertMessage;
    private SessionManagement sessionManagement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sessionManagement = new SessionManagement(getApplicationContext());



        passwordInputLayout = findViewById(R.id.input_layout_password);
        usernameInputLayout = findViewById(R.id.input_layout_username);

        input_name = findViewById(R.id.input_name);
        input_password = findViewById(R.id.input_password);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);

        alertMessage = new AlertDialog.Builder(this).create();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Server ip address:");

// Set up the input
        WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        ip = ip.substring(0, ip.length()-1);

        final EditText input = new EditText(this);
        FrameLayout container = new FrameLayout(this);
        FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        input.setLayoutParams(params);
        container.addView(input);
        input.setText(ip);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(container);

// Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String m_Text = input.getText().toString().trim();
                sessionManagement.setIpAddress(m_Text);

            }
        });
//                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
        builder.setCancelable(false);
        builder.show();

    }

    public void toSignUp(View view) {
        Intent toSignUpActivity = new Intent(this, SignUp.class);
        startActivity(toSignUpActivity);
    }

    public void goToHome(View view) {

        //first getting the values
        final String username = input_name.getText().toString().trim();
        final String password = input_password.getText().toString().trim();

        //validating inputs
        if (TextUtils.isEmpty(password)) {
            passwordInputLayout.setError("Please enter your password");
            passwordInputLayout.requestFocus();
        }

        if (TextUtils.isEmpty(username)) {
            usernameInputLayout.setError("Please enter your username");
            usernameInputLayout.requestFocus();
        }

        if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password)){
            checkLogin(username, password);
        }

    }

    private void checkLogin(String username, String password) {

        progressDialog.setMessage("Logging in...");

        if(!progressDialog.isShowing()) progressDialog.show();

        int param1 = Request.Method.POST;
        String param2 = sessionManagement.getRootIp()+URLs.URL_LOGIN;
        Response.Listener<String> param3 = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(progressDialog.isShowing()) progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    boolean error = jsonObject.getBoolean("error");

                    if(error){
                        alertMessage.setTitle("Incorrect credentials");
                        alertMessage.setMessage("The username or the password you've entered are wrong\nPlease try again");
                        alertMessage.setButton(AlertDialog.BUTTON_NEUTRAL, "Ok", new DialogInterface.OnClickListener(){

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                alertMessage.dismiss();
                            }
                        });
                        alertMessage.setOnShowListener(e -> alertMessage.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.colorPrimaryDark)));
                        alertMessage.show();
                    }else{
                        JSONObject user = jsonObject.getJSONObject("user");
                        String firstName = user.getString("firstName");
                        String lastName = user.getString("lastName");
                        int id = user.getInt("id_user");
                        String email = user.getString("email");

                        sessionManagement.setLogin(true);
                        sessionManagement.setInfos(firstName, lastName, id, email);
//                        saveOnDatabase(id, username);

                        Intent intent = new Intent(getApplicationContext(), ChooserActivity.class);
                        startActivity(intent);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json :"+e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        };

        Response.ErrorListener param4 = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(progressDialog.isShowing()) progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Error occurred due to "+error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        };

        StringRequest stringRequest = new StringRequest(param1, param2, param3, param4) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
