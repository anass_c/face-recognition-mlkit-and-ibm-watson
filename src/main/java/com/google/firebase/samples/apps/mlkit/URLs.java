package com.google.firebase.samples.apps.mlkit;

import android.content.Context;

public class URLs {


    SessionManagement sessionManagement;


    public static final String IP_ADDRESS = "IP";

    private String ROOT_URL; /*  = "http://192.168.8.102/pillsApp/User/";*/
    public static final String URL_REGISTER = "addUser.php";
    public static final String URL_LOGIN = "login.php";

    public URLs(Context context){
        sessionManagement = new SessionManagement(context);
    }

    public String getROOT_URL() {
        return "http://"+sessionManagement.getInfos().get(IP_ADDRESS)+"/pillsApp/User/";
    }

    public String getURL_REGISTER() {
        return getROOT_URL()+URL_REGISTER;
    }

    public String getURL_LOGIN(){
        return getROOT_URL()+URL_LOGIN;
    }
}
